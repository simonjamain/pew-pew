import { GeckosServer, ServerChannel } from "@geckos.io/server";

export class GameServer {
    private static readonly tickRate:number = 128// how many times per seconds the game should send updates to players
    private io:GeckosServer

    constructor(io: GeckosServer) {
        this.io = io
        io.onConnection(channel => {
            channel.onDisconnect(() => {
                console.log(`${channel.id} got disconnected`)
            })
        })

        this.gameStatusUpdate()
    }

    private getMillisecondsBetweenFrames():number {
        return 1000 / GameServer.tickRate;
    }

    gameStatusUpdate = () => {
        const startTime = new Date().getTime();

        this.io.emit('status', {message:"this is a status"});

        const runtime = new Date().getTime() - startTime;
        const nextUpdate = Math.max(0, this.getMillisecondsBetweenFrames()-runtime)
        setTimeout(this.gameStatusUpdate, nextUpdate);
    }
}