import geckos from '@geckos.io/client'
import {FrameRate} from './controlers/FrameRate'

// or add a minified version to your index.html file
// https://github.com/geckosio/geckos.io/tree/master/bundles

const channel = geckos({url: window.location.href, port: undefined})
const frameRate = new FrameRate()

channel.onConnect(error => {
  if (error) {
    console.error(error.message)
    return
  }

  channel.on('status', data => {
    // console.log("You got the status", data)
    frameRate.tick()
    console.log("frameRate", frameRate.getFramerate())
  })
})