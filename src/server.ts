import { GameServer } from './controlers/GameServer.js';
import geckos, { iceServers } from '@geckos.io/server'

import http from 'http'
import express from 'express'
import * as dotenv from 'dotenv'
dotenv.config();
dotenv.config({ path: `.env.local`, override: true });

const app = express()
const server = http.createServer(app)
const options = {
  portRange: {
    min: Number(process.env.UDP_PORT_RANGE_MIN) || 21000,
    max: Number(process.env.UDP_PORT_RANGE_MAX) || 21100
  },
  iceServers
}
const io = geckos(options)
// const io = geckos(
//   {
//     portRange: { min: 20000, max: 21000 },
//     iceServers: process.env.NODE_ENV !== 'production' ? [] : iceServers
//   })

io.addServer(server)

const gameServer = new GameServer(io)

app.use(express.static('public/client'))

server.listen(80)
