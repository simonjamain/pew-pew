# A simple multiplayer browser-based gamedev template

This template is based on this principles :

- typescript
- simple canvas rendering
- asynchronous multiplayer using UDP thanks to [geckos.io](https://github.com/geckosio/geckos.iop)
- a docker-based CD template

# What you need to setup CI/CD

## prerequisites

- an account with rigths to manage **docker** on a server accessible from **ssh**
- that's pretty much it

## setup

set up the following gitlab CI/CD variables:
- `DEPLOYMENT_SSH_PRIVATE_KEY`: the private ssh key of the deployment account
- `DEPLOYMENT_SSH_HOST_PUBLIC_KEY`: the public ssh key of the deployment host
- `APP_NAME`: the app name used for docker image/container name
- `DEPLOYMENT_PORT`: the port that you want your app to be exposed on
- `DEPLOYMENT_SSH_USERATHOST`: the deployment account/host used for ssh deployment
- `UDP_PORT_RANGE_MIN`: the start of the udp range you want to expose (1 port per player)
- `UDP_PORT_RANGE_MAX`:  the end of the udp range you want to expose (1 port per player)

You then may want to use a reverse proxy to expose you app, here is an example of an nginx conf :

```nginx
server {
  listen 80;
  server_name pewpew.simonjamain.fr;
  return 301 https://$host$request_uri;

}

server {
  listen 443 ssl;
  server_name pewpew.simonjamain.fr;
  ssl_certificate         /etc/letsencrypt/live/simonjamain.fr/fullchain.pem;
  ssl_certificate_key     /etc/letsencrypt/live/simonjamain.fr/privkey.pem;

#   access_log            /var/log/nginx/pewpew.access.log;
#   error_log            /var/log/nginx/pewpew.error.log;

  location / {
    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;
    proxy_set_header        Upgrade $http_upgrade;
    proxy_set_header        Connection "upgrade";

    proxy_pass          http://localhost:3003;
    proxy_read_timeout  90;
  }
}
```